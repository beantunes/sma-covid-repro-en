#!/bin/bash

#for i in {1..30}
#do
    # powerjoular measures the power consumption while running example.py
    # Append the result to power_results.txt
    # $! is the PID of pythonRandomGeneration
    
#    ./exe mts000M000000 0 configVaccin configDpt63 &          
#	PROGRAM_PID=$!          
#    
#	powerjoular -p $PROGRAM_PID >> consumptionOn3MinutesDpt63.txt &
#	POWERJOULAR_PID=$!
#	sleep 180
#	kill -INT $POWERJOULAR_PID
#	kill -INT $PROGRAM_PID
#
#
#done

for i in {1..30}
do
    # powerjoular measures the power consumption while running example.py
    # Append the result to power_results.txt
    # $! is the PID of pythonRandomGeneration
    
    ./exe mts000M000000 0 configVaccin configDpt75 &          
	PROGRAM_PID=$!          
    
	powerjoular -p $PROGRAM_PID >> consumptionOn3MinutesDpt75.txt &
	POWERJOULAR_PID=$!
	sleep 180
	kill -INT $POWERJOULAR_PID
	kill -INT $PROGRAM_PID


done

